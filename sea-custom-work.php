<?php
   /*
   Plugin Name: Sea Custom Work
   Plugin URI: http://solzsoft.com
   Description: Sea Custom Work
   Version: 1.1.1.0
   Author: Muhammad Zeeshan
   Author URI: http://solzsoft.com
   License: GPL2
   */

if (!defined('DS')) {
    define("DS", DIRECTORY_SEPARATOR);
}

include(dirname(__FILE__) . DS . 'includes' . DS . 'class.sea.php');
new SEA_Custom_Work();