<?php

class SEA_Custom_Work {

    /**
     * Constructor
     */
    public function __construct() {
        add_action('admin_head', array($this, 'sea_hide_core_update_notice'), 1);
    }

    public function sea_hide_core_update_notice() {
        if (!current_user_can('update_core')) {
            remove_action('admin_notices', 'update_nag', 3);
        }
    }

}
